#Create EC2 instance 
resource "aws_instance" "EC2fromTerraform" {

    ami = "ami-0cca134ec43cf708f"
    instance_type = "t2.micro"

    tags = {
        name = "${var.aws_ec2_name}"
    }
  
}