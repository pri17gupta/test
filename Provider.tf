terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  backend "http" {}
}

# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
}